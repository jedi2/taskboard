import React, { useState } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import CreateTask from './Components/CreateTask';
import Tasks from './Components/Tasks';
import { Progress } from 'reactstrap';



function App() {
  const initialTasks = [
    {
      name: 'Create F1',
      id: Math.random(),
      priority: 1,
      status: 'to do'
    },
    {
      name: 'Create F10',
      id: Math.random(),
      priority: 4,
      status: 'to do'
    },
    {
      name: 'Create F11',
      id: Math.random(),
      priority: 7,
      status: 'to do'
    },
    {
      name: 'Create F1',
      id: Math.random(),
      priority: 2,
      status: 'to do'
    },
    {
      name: 'Create F2',
      id: Math.random(),
      priority: 3,
      status: 'review'
    },
    {
      name: 'Create F3',
      id: Math.random(),
      priority: 6,
      status: 'in progress'
    },
    {
      name: 'Create F4',
      id: Math.random(),
      priority: 5,
      status: 'done'
    }
  ]

  const [tasks, setTasks] = useState(initialTasks);



  const onCreateTask = (taskInput, priorityInput) => {
    const updatedTasks = [...tasks]
    updatedTasks.push({ id: Math.random, name: taskInput, priority: priorityInput, status: 'to do' });
    setTasks(updatedTasks);
  };

  const onDeleteTask = (id) => {
    const updatedTasks = tasks.filter(el => el.id !== id);
    setTasks(updatedTasks);
  }; //  fix issue with deleting all elements

  const onPriorityIncrease = (id) => {
    const index = tasks.findIndex(el => el.id === id);
    const updatedTasks = [...tasks]
    tasks[index].priority = tasks[index].priority + 1
    setTasks(updatedTasks);
  };
  const onPriorityDecrease = (id) => {
    const index = tasks.findIndex(el => el.id === id);
    const updatedTasks = [...tasks]
    tasks[index].priority = tasks[index].priority - 1
    setTasks(updatedTasks);
  };

  const onMoveTaskRight = (taskId) => {
    const updatedTasks = tasks.map(el => {
      if (el.id === taskId) {
        if (el.status === 'to do') {
          return {
            ...el,
            status: 'in progress'
          }
        } else if (el.status === 'in progress') {
          return {
            ...el,
            status: 'review'
          }
        } else
          return {
            ...el,
            status: 'done'
          }
      } else
        return el;
    });
    setTasks(updatedTasks);
  };

  const onMoveTaskLeft = (taskId) => {
    const updatedTasks = tasks.map(el => {
      if (el.id === taskId) {
        if (el.status === 'in progress') {
          return {
            ...el,
            status: 'to do'
          }
        } else if (el.status === 'review') {
          return {
            ...el,
            status: 'in progress'
          }
        } else {
          return {
            ...el,
            status: 'review'
          }
        }
      } else
        return el;
    });
    setTasks(updatedTasks);
  };


  const onSaveTask = (task) => {
    const updatedTasks = tasks.map(el => {
      return el.id === task.id ? { ...el, name: task.name } : el
    });
    setTasks(updatedTasks)
  };
  // const onSavePriority = (priority) => {
  //   const updatedTasks = tasks.map(el => {
  //     return el.id === priority.id ? { ...el, priority: priority.priority } : el
  //   });
  //   setTasks(updatedTasks)
  // };


  return (
    <div className="App bg-light">
      <h1>Kanban</h1>
      <br/>
      <CreateTask onCreateTask={onCreateTask} />
      <br/>
      <Progress multi>
        <Progress bar color="primary"  value={tasks.filter(el => el.status === 'to do').length}>To do</Progress>
        <Progress bar color="success" value={tasks.filter(el => el.status === 'done').length}>Done</Progress>
        <Progress bar color="info" value={tasks.filter(el => el.status === 'review').length}>Review</Progress>
        <Progress bar color="warning" value={tasks.filter(el => el.status === 'in progress').length}>In progress</Progress>
      </Progress>
      <br/>

    
      <Tasks tasks={tasks}
        onDeleteTask={onDeleteTask}
        onSaveTask={onSaveTask}
        onPriorityIncrease={onPriorityIncrease}
        onPriorityDecrease={onPriorityDecrease}
        onMoveTaskRight={onMoveTaskRight}
        onMoveTaskLeft={onMoveTaskLeft}
      />

    </div>
  );
}

export default App;
