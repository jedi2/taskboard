import React, { useState } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';

function CreateStatus(props) {
    const [isOpenCreateForm, setisOpenCreateForm] = useState('');
    const [isActiveButtonTaskCreate, setIsActiveButtonTaskCreate] = useState(false);
    const [statusInput, setStatusInput] = useState('');
    const [taskInput, setTaskInput] = useState('');
    const [priorityInput, setPriorityInput] = useState('');

    const openCreateTaskForm = () => {
        setisOpenCreateForm(true);
    }

    const onStatusChange = (e) => {
        setIsActiveButtonTaskCreate(e.target.value > 0)
        setStatusInput(e.target.value)

    };
    const onPriorityChange = (e) => {
        setIsActiveButtonTaskCreate(e.target.value > 0 && e.target.value<10)
        setPriorityInput(e.target.value)
    };
    const onTaskChange = (e) => {
        setIsActiveButtonTaskCreate(e.target.value>0)
        setTaskInput(e.target.value)
    };

    const statusSave = (e) => {
        e.preventDefault();
        props.onCreatingStatus(taskInput, statusInput, priorityInput);
        taskReset();
    };
    const taskReset = () => {
        setTaskInput('');
        setStatusInput('');
        setPriorityInput('');
        setisOpenCreateForm(false);
        setIsActiveButtonTaskCreate(false);
    };



    return (
        <div>
            {!isOpenCreateForm ? <button className="btn btn-primary" onClick={openCreateTaskForm}>Create task</button> : null}
            {isOpenCreateForm && <form>
                <div>
                    <label>Task</label>
                    <input type="text" className="form-control" value={taskInput} onChange={onTaskChange} />
                </div>
                <div>
                    <label>Status</label>
                    <input type="text" className="form-control" value={statusInput} onChange={onStatusChange} />
                </div>
                <div className="form-group">
                    <label>Priority</label>
                    <input type="number" className="form-control" value={priorityInput} onChange={onPriorityChange} />
                </div>
                <button type="btn" className="btn btn-primary" disabled={!isActiveButtonTaskCreate} onClick={statusSave}>Save</button>
                <button type="btn" className="btn btn-secondary" onClick={taskReset}>Cancel</button>
            </form>
            }
        </div >
    )
};

export default CreateStatus;
