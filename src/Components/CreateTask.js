import React, { useState } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';

function CreateTask(props) {
    const [isOpenCreateForm, setisOpenCreateForm] = useState('');
    const [isActiveButtonTaskCreate, setIsActiveButtonTaskCreate] = useState(false);
    const [taskInput, setTaskInput] = useState('');
    const [priorityInput, setPriorityInput] = useState('');


    const openCreateTaskForm = () => {
        setisOpenCreateForm(true);
    }
    const onTaskChange = (e) => {
        setIsActiveButtonTaskCreate(e.target.value.length > 4 && (Number(priorityInput) > 0 || Number(priorityInput)<10));
        setTaskInput(e.target.value)
    };
    const onPriorityChange = (e) => {
        setIsActiveButtonTaskCreate((Number(e.target.value) > 0 && Number(e.target.value) < 10) && taskInput.length>4); //alert warning widow
        setPriorityInput(e.target.value)
    }
    const taskSubmit = (e) => {
        e.preventDefault();
        props.onCreateTask(taskInput, priorityInput);
        taskReset();
    };
    const taskReset = () => {
        setTaskInput('');
        setPriorityInput('')
        setisOpenCreateForm(false);
        setIsActiveButtonTaskCreate(false);
    };

    return (
        <div>
            {!isOpenCreateForm ? <button className="btn btn-primary" onClick={openCreateTaskForm}>New Task</button> : null}
            {isOpenCreateForm &&
                <form>
                    <div className="form-group">
                        <label>Task</label>
                        <input type="text" className="form-control" value={taskInput} onChange={onTaskChange} />
                    </div>
                    <div className="form-group">
                        <label>Priority</label>
                        <input type="number" className="form-control" value={priorityInput} onChange={onPriorityChange} />
                    </div>
                    <button type="submit" className="btn btn-primary" disabled={!isActiveButtonTaskCreate} onClick={taskSubmit}>Submit</button>
                    <button type="btn" className="btn btn-secondary" onClick={taskReset}>Cancel</button>
                </form>
            }
        </div>
    )
};
export default CreateTask;