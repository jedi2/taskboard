import React, { useState } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';


function Tasks(props) {

    const todoTasks = props.tasks.filter(el => el.status === 'to do').sort((a, b) => a.priority - b.priority);
    const inprogressTasks = props.tasks.filter(el => el.status === 'in progress').sort((a, b) => a.priority - b.priority);
    const reviewTasks = props.tasks.filter(el => el.status === 'review').sort((a, b) => a.priority - b.priority);
    const doneTasks = props.tasks.filter(el => el.status === 'done').sort((a, b) => a.priority - b.priority);



    const [taskEdit, setTaskEdit] = useState({});
    // const [priorityEdit, setPriorityEdit] = useState({});


    const editMode = (task) => {
        setTaskEdit(task);

    };
    // const editPriorityMode = (priority) => {
    //     setPriorityEdit(priority)
    // }

    const onEditTaskChange = (e) => {
        setTaskEdit({ ...taskEdit, name: e.target.value });

    };
    // const onEditPriorityChange = (e) => {
    //     setPriorityEdit({ ...priorityEdit, priority: e.target.value })
    // }

    const taskSave = () => {
        props.onSaveTask(taskEdit);
        setTaskEdit({});
    };
    // const prioritySave = () => {
    //     props.onSavePriority(priorityEdit)
    //     setPriorityEdit({});
    // };

    //Form
    
    const [isActiveButtonTaskEdit, setIsActiveButtonTaskEdit] = useState(false);


    const taskEditReset = () => {
        setTaskEdit({});
        setIsActiveButtonTaskEdit(false);
    };



    return (

        <div>
            <div className="container">
                <div className="row">
                    <div className="col-sm container">
                        <h4 className="border-bottom-0">To do</h4>
                        {todoTasks
                            .map(el => <ul className=" border border-primary list-group mt-3 mb-2 " key={el.id}>
                                <li className="list-group-item"><strong>
                                    {
                                        taskEdit.id === el.id
                                            ? <>
                                                <form>
                                                    <div className="form-group">
                                                        <label>Task</label>
                                                        <input type="text" value={taskEdit.name} onChange={onEditTaskChange} />
                                                    </div>
                                                    <button type="btn" className="btn btn-primary" onClick={taskSave} disabled={taskEdit.name.trim() === '' && !isActiveButtonTaskEdit}>Save</button>
                                                    <button type="btn" className="btn btn-secondary" onClick={taskEditReset}>Cancel</button>
                                                </form>
                                            </>
                                            :
                                            <div class="row">
                                                <div className="col-8">{el.name}</div>

                                                <div className="col-4"><button onClick={() => editMode(el)} className="btn btn-sm btn-light"><svg className="bi bi-pencil" width="1.5em" height="1.5em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path fillRule="evenodd" d="M11.293 1.293a1 1 0 0 1 1.414 0l2 2a1 1 0 0 1 0 1.414l-9 9a1 1 0 0 1-.39.242l-3 1a1 1 0 0 1-1.266-1.265l1-3a1 1 0 0 1 .242-.391l9-9zM12 2l2 2-9 9-3 1 1-3 9-9z" />
                                                    <path fillRule="evenodd" d="M12.146 6.354l-2.5-2.5.708-.708 2.5 2.5-.707.708zM3 10v.5a.5.5 0 0 0 .5.5H4v.5a.5.5 0 0 0 .5.5H5v.5a.5.5 0 0 0 .5.5H6v-1.5a.5.5 0 0 0-.5-.5H5v-.5a.5.5 0 0 0-.5-.5H3z" />
                                                </svg></button></div>
                                            </div>

                                    }
                                    {/* {
                                        taskEdit.id === el.id
                                            ? <>
                                                <input type="number" value={priorityEdit.priority} onChange={onEditPriorityChange} />
                                                <button className="btn btn-success" onClick={prioritySave}>Save</button>
                                            </>
                                            :
                                            <span onClick={() => editPriorityMode(el)}>{el.priority}</span>
                                    } */}
                                </strong><br /><em>Priority:
                                  <button className="btn btn-sm" onClick={() => props.onPriorityDecrease(el.id)}><svg className="bi bi-dash" width="2em" height="2em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                            <path fillRule="evenodd" d="M3.5 8a.5.5 0 0 1 .5-.5h8a.5.5 0 0 1 0 1H4a.5.5 0 0 1-.5-.5z" />
                                        </svg></button>
                                        {el.priority}
                                        <button className="btn btn-sm" onClick={() => props.onPriorityIncrease(el.id)}>
                                            <svg className="bi bi-plus" width="2em" height="2em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path fillRule="evenodd" d="M8 3.5a.5.5 0 0 1 .5.5v4a.5.5 0 0 1-.5.5H4a.5.5 0 0 1 0-1h3.5V4a.5.5 0 0 1 .5-.5z" />
                                                <path fillRule="evenodd" d="M7.5 8a.5.5 0 0 1 .5-.5h4a.5.5 0 0 1 0 1H8.5V12a.5.5 0 0 1-1 0V8z" />
                                            </svg>
                                        </button>
                                    </em> <br />



                                    <hr />
                                    {
                                        el.status !== 'to do' &&
                                        <button type="button"
                                            className="btn btn-sm"
                                            onClick={() => props.onMoveTaskLeft(el.id)}><svg className="bi bi-arrow-left" width="2em" height="2em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path fillRule="evenodd" d="M5.854 4.646a.5.5 0 0 1 0 .708L3.207 8l2.647 2.646a.5.5 0 0 1-.708.708l-3-3a.5.5 0 0 1 0-.708l3-3a.5.5 0 0 1 .708 0z" />
                                                <path fillRule="evenodd" d="M2.5 8a.5.5 0 0 1 .5-.5h10.5a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5z" />
                                            </svg>

                                        </button>
                                    }
                                    <button onClick={() => props.onDeleteTask(el.id)} className="btn btn-danger btn-sm mr-5"><svg className="bi bi-trash-fill" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                        <path fillRule="evenodd" d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5a.5.5 0 0 0-1 0v7a.5.5 0 0 0 1 0v-7z" />
                                    </svg></button>
                                    {
                                        el.status !== 'done' &&
                                        <button type="button"
                                            className="btn btn-sm"
                                            onClick={() => props.onMoveTaskRight(el.id)}> <svg className="bi bi-arrow-right" width="2em" height="2em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path fillRule="evenodd" d="M10.146 4.646a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708-.708L12.793 8l-2.647-2.646a.5.5 0 0 1 0-.708z" />
                                                <path fillRule="evenodd" d="M2 8a.5.5 0 0 1 .5-.5H13a.5.5 0 0 1 0 1H2.5A.5.5 0 0 1 2 8z" />
                                            </svg>
                                        </button>
                                    }

                                </li></ul>)
                        }
                    </div>


                    <div className="col-sm">
                        <h4>In progress</h4>
                        {inprogressTasks
                            .map(el => <ul className=" border border-warning list-group mt-3 mb-2 " key={el.id}>
                                <li className="list-group-item"><strong>
                                    {
                                        taskEdit.id === el.id
                                            ? <>
                                                <form>
                                                    <div className="form-group">
                                                        <label>Task</label>
                                                        <input type="text" value={taskEdit.name} onChange={onEditTaskChange} />
                                                    </div>
                                                    <button type="btn" className="btn btn-primary" onClick={taskSave} disabled={taskEdit.name.trim() === '' && !isActiveButtonTaskEdit}>Save</button>
                                                    <button type="btn" className="btn btn-secondary" onClick={taskEditReset}>Cancel</button>
                                                </form>
                                            </>
                                            :
                                            <div class="row">
                                                <div className="col-8">{el.name}</div>

                                                <div className="col-4"><button onClick={() => editMode(el)} className="btn btn-sm btn-light"><svg className="bi bi-pencil" width="1.5em" height="1.5em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path fillRule="evenodd" d="M11.293 1.293a1 1 0 0 1 1.414 0l2 2a1 1 0 0 1 0 1.414l-9 9a1 1 0 0 1-.39.242l-3 1a1 1 0 0 1-1.266-1.265l1-3a1 1 0 0 1 .242-.391l9-9zM12 2l2 2-9 9-3 1 1-3 9-9z" />
                                                    <path fillRule="evenodd" d="M12.146 6.354l-2.5-2.5.708-.708 2.5 2.5-.707.708zM3 10v.5a.5.5 0 0 0 .5.5H4v.5a.5.5 0 0 0 .5.5H5v.5a.5.5 0 0 0 .5.5H6v-1.5a.5.5 0 0 0-.5-.5H5v-.5a.5.5 0 0 0-.5-.5H3z" />
                                                </svg></button></div>
                                            </div>

                                    }
                                    {/* {
                                    taskEdit.id === el.id
                                        ? <>
                                            <input type="number" value={priorityEdit.priority} onChange={onEditPriorityChange} />
                                            <button className="btn btn-success" onClick={prioritySave}>Save</button>
                                        </>
                                        :
                                        <span onClick={() => editPriorityMode(el)}>{el.priority}</span>
                                } */}
                                </strong><br /><em>Priority:
                              <button className="btn btn-sm" onClick={() => props.onPriorityDecrease(el.id)}><svg className="bi bi-dash" width="2em" height="2em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                            <path fillRule="evenodd" d="M3.5 8a.5.5 0 0 1 .5-.5h8a.5.5 0 0 1 0 1H4a.5.5 0 0 1-.5-.5z" />
                                        </svg></button>
                                        {el.priority}
                                        <button className="btn btn-sm" onClick={() => props.onPriorityIncrease(el.id)}>
                                            <svg className="bi bi-plus" width="2em" height="2em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path fillRule="evenodd" d="M8 3.5a.5.5 0 0 1 .5.5v4a.5.5 0 0 1-.5.5H4a.5.5 0 0 1 0-1h3.5V4a.5.5 0 0 1 .5-.5z" />
                                                <path fillRule="evenodd" d="M7.5 8a.5.5 0 0 1 .5-.5h4a.5.5 0 0 1 0 1H8.5V12a.5.5 0 0 1-1 0V8z" />
                                            </svg>
                                        </button>
                                    </em> <hr />

                                    {
                                        el.status !== 'to do' &&
                                        <button type="button"
                                            className="btn btn-sm "
                                            onClick={() => props.onMoveTaskLeft(el.id)}><svg className="bi bi-arrow-left" width="2em" height="2em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path fillRule="evenodd" d="M5.854 4.646a.5.5 0 0 1 0 .708L3.207 8l2.647 2.646a.5.5 0 0 1-.708.708l-3-3a.5.5 0 0 1 0-.708l3-3a.5.5 0 0 1 .708 0z" />
                                                <path fillRule="evenodd" d="M2.5 8a.5.5 0 0 1 .5-.5h10.5a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5z" />
                                            </svg>

                                        </button>
                                    }
                                    <button onClick={() => props.onDeleteTask(el.id)} className="btn btn-danger btn-sm ml-3 mr-3"><svg className="bi bi-trash-fill" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                        <path fillRule="evenodd" d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5a.5.5 0 0 0-1 0v7a.5.5 0 0 0 1 0v-7z" />
                                    </svg></button>
                                    {
                                        el.status !== 'done' &&
                                        <button type="button"
                                            className="btn btn-sm"
                                            onClick={() => props.onMoveTaskRight(el.id)}> <svg className="bi bi-arrow-right" width="2em" height="2em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path fillRule="evenodd" d="M10.146 4.646a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708-.708L12.793 8l-2.647-2.646a.5.5 0 0 1 0-.708z" />
                                                <path fillRule="evenodd" d="M2 8a.5.5 0 0 1 .5-.5H13a.5.5 0 0 1 0 1H2.5A.5.5 0 0 1 2 8z" />
                                            </svg>
                                        </button>
                                    }
                                </li></ul>)
                        }
                    </div>
                    <div className="col-sm">
                        <h4>Review</h4>
                        {reviewTasks
                            .map(el => <ul className=" border border-info list-group mt-3 mb-2 " key={el.id}>
                                <li className="list-group-item"><strong>
                                    {
                                        taskEdit.id === el.id
                                            ? <>
                                                <form>
                                                    <div className="form-group">
                                                        <label>Task</label>
                                                        <input type="text" value={taskEdit.name} onChange={onEditTaskChange} />
                                                    </div>
                                                    <button type="btn" className="btn btn-primary" onClick={taskSave} disabled={taskEdit.name.trim() === '' && !isActiveButtonTaskEdit}>Save</button>
                                                    <button type="btn" className="btn btn-secondary" onClick={taskEditReset}>Cancel</button>
                                                </form>
                                            </>
                                            :
                                            <div class="row">
                                                <div className="col-8">{el.name}</div>

                                                <div className="col-4"><button onClick={() => editMode(el)} className="btn btn-sm btn-light"><svg className="bi bi-pencil" width="1.5em" height="1.5em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path fillRule="evenodd" d="M11.293 1.293a1 1 0 0 1 1.414 0l2 2a1 1 0 0 1 0 1.414l-9 9a1 1 0 0 1-.39.242l-3 1a1 1 0 0 1-1.266-1.265l1-3a1 1 0 0 1 .242-.391l9-9zM12 2l2 2-9 9-3 1 1-3 9-9z" />
                                                    <path fillRule="evenodd" d="M12.146 6.354l-2.5-2.5.708-.708 2.5 2.5-.707.708zM3 10v.5a.5.5 0 0 0 .5.5H4v.5a.5.5 0 0 0 .5.5H5v.5a.5.5 0 0 0 .5.5H6v-1.5a.5.5 0 0 0-.5-.5H5v-.5a.5.5 0 0 0-.5-.5H3z" />
                                                </svg></button></div>
                                            </div>

                                    }
                                    {/* {
                                    taskEdit.id === el.id
                                        ? <>
                                            <input type="number" value={priorityEdit.priority} onChange={onEditPriorityChange} />
                                            <button className="btn btn-success" onClick={prioritySave}>Save</button>
                                        </>
                                        :
                                        <span onClick={() => editPriorityMode(el)}>{el.priority}</span>
                                } */}
                                </strong><br /><em>Priority:
                              <button className="btn btn-sm" onClick={() => props.onPriorityDecrease(el.id)}><svg className="bi bi-dash" width="2em" height="2em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                            <path fillRule="evenodd" d="M3.5 8a.5.5 0 0 1 .5-.5h8a.5.5 0 0 1 0 1H4a.5.5 0 0 1-.5-.5z" />
                                        </svg></button>
                                        {el.priority}
                                        <button className="btn btn-sm" onClick={() => props.onPriorityIncrease(el.id)}>
                                            <svg className="bi bi-plus" width="2em" height="2em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path fillRule="evenodd" d="M8 3.5a.5.5 0 0 1 .5.5v4a.5.5 0 0 1-.5.5H4a.5.5 0 0 1 0-1h3.5V4a.5.5 0 0 1 .5-.5z" />
                                                <path fillRule="evenodd" d="M7.5 8a.5.5 0 0 1 .5-.5h4a.5.5 0 0 1 0 1H8.5V12a.5.5 0 0 1-1 0V8z" />
                                            </svg>
                                        </button>
                                    </em> <hr />

                                    {
                                        el.status !== 'to do' &&
                                        <button type="button"
                                            className="btn btn-sm "
                                            onClick={() => props.onMoveTaskLeft(el.id)}><svg className="bi bi-arrow-left" width="2em" height="2em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path fillRule="evenodd" d="M5.854 4.646a.5.5 0 0 1 0 .708L3.207 8l2.647 2.646a.5.5 0 0 1-.708.708l-3-3a.5.5 0 0 1 0-.708l3-3a.5.5 0 0 1 .708 0z" />
                                                <path fillRule="evenodd" d="M2.5 8a.5.5 0 0 1 .5-.5h10.5a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5z" />
                                            </svg>

                                        </button>
                                    }
                                    <button onClick={() => props.onDeleteTask(el.id)} className="btn btn-danger btn-sm ml-3 mr-3"><svg className="bi bi-trash-fill" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                        <path fillRule="evenodd" d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5a.5.5 0 0 0-1 0v7a.5.5 0 0 0 1 0v-7z" />
                                    </svg></button>
                                    {
                                        el.status !== 'done' &&
                                        <button type="button"
                                            className="btn btn-sm"
                                            onClick={() => props.onMoveTaskRight(el.id)}> <svg className="bi bi-arrow-right" width="2em" height="2em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path fillRule="evenodd" d="M10.146 4.646a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708-.708L12.793 8l-2.647-2.646a.5.5 0 0 1 0-.708z" />
                                                <path fillRule="evenodd" d="M2 8a.5.5 0 0 1 .5-.5H13a.5.5 0 0 1 0 1H2.5A.5.5 0 0 1 2 8z" />
                                            </svg>
                                        </button>
                                    }
                                </li></ul>)
                        }
                    </div>
                    <div className="col-sm">
                        <h4>Done</h4>
                        {doneTasks.map(el => <ul className=" border border-success list-group mt-3 mb-2 " key={el.id}>
                            <li className="list-group-item"><strong>
                                {
                                    taskEdit.id === el.id
                                        ? <>
                                            <form>
                                                <div className="form-group">
                                                    <label>Task</label>
                                                    <input type="text" value={taskEdit.name} onChange={onEditTaskChange} />
                                                </div>
                                                <button type="btn" className="btn btn-primary" onClick={taskSave} disabled={taskEdit.name.trim() === '' && !isActiveButtonTaskEdit}>Save</button>
                                                <button type="btn" className="btn btn-secondary" onClick={taskEditReset}>Cancel</button>
                                            </form>
                                        </>
                                        :
                                        <div class="row">
                                            <div className="col-8">{el.name}</div>

                                            <div className="col-4"><button onClick={() => editMode(el)} className="btn btn-sm btn-light"><svg className="bi bi-pencil" width="1.5em" height="1.5em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path fillRule="evenodd" d="M11.293 1.293a1 1 0 0 1 1.414 0l2 2a1 1 0 0 1 0 1.414l-9 9a1 1 0 0 1-.39.242l-3 1a1 1 0 0 1-1.266-1.265l1-3a1 1 0 0 1 .242-.391l9-9zM12 2l2 2-9 9-3 1 1-3 9-9z" />
                                                <path fillRule="evenodd" d="M12.146 6.354l-2.5-2.5.708-.708 2.5 2.5-.707.708zM3 10v.5a.5.5 0 0 0 .5.5H4v.5a.5.5 0 0 0 .5.5H5v.5a.5.5 0 0 0 .5.5H6v-1.5a.5.5 0 0 0-.5-.5H5v-.5a.5.5 0 0 0-.5-.5H3z" />
                                            </svg></button></div>
                                        </div>

                                }
                                {/* {
                                        taskEdit.id === el.id
                                            ? <>
                                                <input type="number" value={priorityEdit.priority} onChange={onEditPriorityChange} />
                                                <button className="btn btn-success" onClick={prioritySave}>Save</button>
                                            </>
                                            :
                                            <span onClick={() => editPriorityMode(el)}>{el.priority}</span>
                                    } */}
                            </strong><br /><em>Priority:
                                  <button className="btn btn-sm" onClick={() => props.onPriorityDecrease(el.id)}><svg className="bi bi-dash" width="2em" height="2em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                        <path fillRule="evenodd" d="M3.5 8a.5.5 0 0 1 .5-.5h8a.5.5 0 0 1 0 1H4a.5.5 0 0 1-.5-.5z" />
                                    </svg></button>
                                    {el.priority}
                                    <button className="btn btn-sm" onClick={() => props.onPriorityIncrease(el.id)}>
                                        <svg className="bi bi-plus" width="2em" height="2em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                            <path fillRule="evenodd" d="M8 3.5a.5.5 0 0 1 .5.5v4a.5.5 0 0 1-.5.5H4a.5.5 0 0 1 0-1h3.5V4a.5.5 0 0 1 .5-.5z" />
                                            <path fillRule="evenodd" d="M7.5 8a.5.5 0 0 1 .5-.5h4a.5.5 0 0 1 0 1H8.5V12a.5.5 0 0 1-1 0V8z" />
                                        </svg>
                                    </button>
                                </em> <hr />

                                {
                                    el.status !== 'to do' &&
                                    <button type="button"
                                        className="btn btn-sm "
                                        onClick={() => props.onMoveTaskLeft(el.id)}><svg className="bi bi-arrow-left" width="2em" height="2em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                            <path fillRule="evenodd" d="M5.854 4.646a.5.5 0 0 1 0 .708L3.207 8l2.647 2.646a.5.5 0 0 1-.708.708l-3-3a.5.5 0 0 1 0-.708l3-3a.5.5 0 0 1 .708 0z" />
                                            <path fillRule="evenodd" d="M2.5 8a.5.5 0 0 1 .5-.5h10.5a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5z" />
                                        </svg>

                                    </button>
                                }
                                <button onClick={() => props.onDeleteTask(el.id)} className="btn btn-danger btn-sm ml-5"><svg className="bi bi-trash-fill" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                    <path fillRule="evenodd" d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5a.5.5 0 0 0-1 0v7a.5.5 0 0 0 1 0v-7z" />
                                </svg></button>
                                {
                                    el.status !== 'done' &&
                                    <button type="button"
                                        className="btn btn-sm"
                                        onClick={() => props.onMoveTaskRight(el.id)}> <svg className="bi bi-arrow-right" width="2em" height="2em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                            <path fillRule="evenodd" d="M10.146 4.646a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708-.708L12.793 8l-2.647-2.646a.5.5 0 0 1 0-.708z" />
                                            <path fillRule="evenodd" d="M2 8a.5.5 0 0 1 .5-.5H13a.5.5 0 0 1 0 1H2.5A.5.5 0 0 1 2 8z" />
                                        </svg>
                                    </button>
                                }
                            </li></ul>)

                        }
                    </div>
                </div>
            </div>
        </div>
    )
};
export default Tasks;
